import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';
import './App.css';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import Navbar from './components/Navbar';
import ProductList from './components/ProductList';
import Detail from './components/Detail';
import Cart from './components/cart/Cart';
import Default from './components/Default';
import Modal from './components/Modal';
import Payment from './components/Payment';
class App extends Component {
  render() {
    return (
      <React.Fragment>
        <div className="App">
          <Navbar />
          <Switch>
            <Route exact path="/" component={ProductList}></Route>
            <Route path="/detail" component={Detail}></Route>
            <Route path="/cart" component={Cart}></Route>
            <Route path="/payment-success" component={Payment}></Route>
            <Route path="/:default" component={Default}></Route>

          </Switch>
          <Modal />
        </div>
      </React.Fragment>
    );
  }

}

export default App;

// ad113fc4901f923dea3f4cafb2fb33da951504f1890d992c