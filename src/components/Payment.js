import React from 'react';
import { Link } from 'react-router-dom';

export default function Payment() {
  return (
    <div className="row align-items-center justify-content-center">
      <div className="col-6 mx-auto my-5">
        <h3 className="text-success text-center font-weight-bolf">
          Payment Success
        </h3>
        <Link to="/" className="text-center">
          <button className="btn btn-primary text-center btn-lg">
            <i className="fas fa-chevron-left"></i> Continue Shopping
        </button>
        </Link>
      </div>
    </div>
  )
}
