import React from 'react'

export default function Title({ name, title }) {
  return (
    <div className="row">
      <div className="col-md-10 m-auto text-center text-title">
        <h1 className="text-capitalize font-weight-bold text-blue my-5">
          {name} <strong>{title}</strong>
        </h1>
      </div>
    </div>
  )
}
