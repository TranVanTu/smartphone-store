import styled from 'styled-components';
export const ButtonContainer = styled.button`
  text-transform : capitalize;
  font-size : 1.4rem;
  background : transparent;
  color : #999;
  border : #ccc 1px solid;
  border-color : ${props => props.cart ? "var(--main-yellow)" : "var(--main-blue)"};
  border-radius : 5px;
  padding : 0.2rem 0.6rem;
  transition : all 0.5s ease;
  &:hover {
    background-color : var(--light-blue);
    color : var(--main-white);
  }
  &:focus {
    outline : none;
  }
`;
