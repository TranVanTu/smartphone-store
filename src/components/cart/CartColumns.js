import React from 'react'

export default function CartColumns() {
  return (
    <div className="container d-none text-center d-lg-block">
      <div className="row">
        <div className="col-lg-2">
          PRODUCTS
        </div>  
        <div className="col-lg-2">
          PRODUCTS NAME
        </div> 
        <div className="col-lg-2">
          PRICE
        </div> 
        <div className="col-lg-2">
          QUANTITY
        </div> 
        <div className="col-lg-2">
          REMOVE
        </div> 
        <div className="col-lg-2">
          TOTAL
        </div> 
      </div>
    </div>
  )
}
