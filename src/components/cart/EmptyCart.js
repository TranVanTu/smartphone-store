import React from 'react'
import Title from '../Title';
export default function EmptyCart() {
  return (
    <div className="container text-center mx-auto">
      <Title name="Your cart" title="is currently empty" />
    </div>
  );
}
