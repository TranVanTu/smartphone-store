import React from 'react';
export default function CartItem({ item, value }) {
  const { id, title, img, price, total, count } = item;
  const { increment, decrement, removeItem } = value;
  return (
    <div style={{ lineHeight: "2.1 !important" }} className="row my-5 text-capitalize text-center align-items-center large">
      <div className="col-lg-2">
        <img src={img} style={{ width: '80px', height: "70px" }} alt={title} className="img-fluid" />
      </div>
      <div className="col-lg-2">
        <span className="d-lg-none">Product :</span> {title}
      </div>
      <div className="col-lg-2 text-danger font-weight-bold">
        $ {price}
      </div>
      <div className="col-lg-2">
        <span onClick={() => decrement(id)} className="btn btn-outline-secondary btn-sm px-3 font-weight-bold mx-3">-</span>
        {count}
        <span onClick={() => increment(id)} className="btn btn-outline-secondary btn-sm px-3 font-weight-bold mx-3">+</span>
      </div>
      <div className="col-lg-2">
        <button className="btn btn-danger" onClick={() => removeItem(id)}>
          <i className="fas fa-trash"></i>
        </button>
      </div>
      <div className="col-lg-2">
        <span className="d-lg-none">Item Total</span> {total}
      </div>
    </div>
  );
}
