import React from 'react';
import { Link } from 'react-router-dom';

export default function CartTotals({ value }) {
  const { cartSubTotal, cartTax, cartTotal, clearCart } = value;

  return (
    <React.Fragment>
      <div className="container">
        <div className="row">
          <div className="col-12 col-md-4 ml-md-auto text-right">
            <Link to="/">
              <button className="btn btn-outline-warning"
                type="button"
                onClick={() => clearCart()}
              >
                Clear Cart
              </button>
            </Link>
            <h5 className="my-3">
              Subtotal : $<strong>{cartSubTotal}</strong>
            </h5>
            <h5 className="my-3">
              Tax : $<strong>{cartTax}</strong>
            </h5>
            <h5 className="my-3">
              Total : $<strong>{cartTotal}</strong>
            </h5>
            <Link to="/payment-success">
              <button className="btn btn-danger">
                Checkout
              </button>
            </Link>
            <p className="text-muted my-3">
              Click checkout to get the product
            </p>
          </div>
        </div>
      </div>
    </React.Fragment>
  )
}

