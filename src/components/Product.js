import React, { Component } from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import { ProductConsumer } from '../context';
import PropTypes from 'prop-types';
class Product extends Component {
  render() {
    const { id, title, img, price, inCart } = this.props.product;
    return (
      <ProductWrapper className="col-9 mx-auto col-md-3 col-sm-6">
        <div className="card">
          <ProductConsumer>
            {(value) =>
              (
                <div onClick={() => value.handleDetail(id)} className="img-container">
                  <Link to="/detail">
                    <img src={img} alt="product detail" className="card-img-top" />
                  </Link>
                  <button
                    className="btn-cart"
                    disabled={inCart ? true : false}
                    onClick={() => {
                      value.openModal(id);
                    }
                    }
                  >
                    {inCart ? (<p className="text-capitalize mb-0" disabled>Is In Cart</p>) : (<i className="fas fa-cart-plus"></i>)}
                  </button>
                </div>
              )
            }
          </ProductConsumer>
          <div className="card-footer">
            <div className="d-flex justify-content-between align-items-center">
              <p>
                {title}
              </p>
              <h4 className="text-danger font-weight-bold">
                $ {price}
              </h4>
            </div>
          </div>
        </div>
      </ProductWrapper>
    )
  }
}

const ProductWrapper = styled.div`
  background-color : #fff;
  border-radius : 5px;
  padding : 15px;
  margin : 2rem 0;
  transition : 0.4s all ease;
  .card {
    overflow : hidden;
  }
  .card-img-top {
    width : 100%;
    height : 160px;
  }
  &:hover {
    box-shadow : 0 3px 5px rgba(0,0,0,0.4), 0 -3px 5px rgba(0,0,0,0.4);
    transform : scale(1.01);
  }
  .btn-cart {
	border: none;
	background-color: var(--main-yellow);
	padding: 8px 14px;
	border-radius: 4px;
	position: absolute;
	transition: all 0.4s ease-in-out;
	bottom: 5px;
	right: 5px;
	transform: translateY(100px);
  }
  .img-container {
	position: relative;
  overflow: hidden;
  transition: all 0.4s ease-in-out;
  }
  .img-container:hover .btn-cart {
	transform: translateY(0);
  }
  .img-container:hover {
    transform :scale(1.05);
  }
`;

Product.propTypes = {
  product: PropTypes.shape({
    id: PropTypes.number,
    img: PropTypes.string,
    title: PropTypes.string,
    price: PropTypes.number,
    inCart: PropTypes.bool
  })
}

export default Product;
