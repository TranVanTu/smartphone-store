import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import logo from '../logo.png';
import { ButtonContainer } from './Button';
import styled from 'styled-components';
class Navbar extends Component {
  render() {
    return (
      <NavWrapper className="navbar navbar-expand-sm navbar-primary bg-primary px-sm-5">
        <Link to="/">
          <img src={logo} alt="Main logo" className="nav-brand" />
        </Link>
        <ul className="navbar-nav align-items-center">
          <li className="nav-item ml-5">
            <Link className="nav-link text-white font-weight-bold" to="/">
              Products
            </Link>
          </li>
        </ul>
        <Link to="/cart" className="ml-auto">
          <ButtonContainer className="">
            My Cart <i className="fas fa-cart-plus"></i>
          </ButtonContainer>
        </Link>
      </NavWrapper>
    );
  }
}

const NavWrapper = styled.nav`
  background-color : var(--main-blue) !important;
  .nav-link {
    color : var(--main-white) !important;
    font-size : 1.3rem;
    text-transform :capitalize;
  }
`;

export default Navbar;
