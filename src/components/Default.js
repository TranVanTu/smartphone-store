import React, { Component } from 'react';

class Default extends Component {
  render() {
    return (
      <div className="container">
        <div className="row">
          <div className="col-10 mx-auto my-5 col-md-6">
            <h1>
              404 Error : Page Not Found
            </h1>
          </div>
        </div>
      </div>
    )
  }
}

export default Default
