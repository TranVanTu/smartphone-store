import React, { Component } from 'react'
import { ProductConsumer } from '../context';
import { Link } from 'react-router-dom';
import { ButtonContainer } from './Button';

class Detail extends Component {
  render() {
    return (
      <ProductConsumer>
        {(value) => {
          const { id, company, img, info, price, title, inCart } = value.detailProduct;
          console.log(value);
          return (
            <div className="container py-5">
              <div className="row">
                <div className="col-md-10 mx-auto text-center">
                  <h1 className="my-4">
                    {title}
                  </h1>
                </div>
              </div>
              <div className="row">
                <div className="col-md-5">
                  <img src={img} className="img-fluid align-items-center" alt="Detail Product" />
                </div>
                <div className="col-md-7">
                  <h2>
                    Model : {title}
                  </h2>
                  <h4>
                    Made by : {company}
                  </h4>
                  <h4 className="text-danger">
                    Price <strong>$ {price} </strong>
                  </h4>
                  <p className="text-capitalize">
                    <strong>Some info about product : </strong> {info}
                  </p>
                  <div className="bg-secondary d-flex justify-content-between rounded p-2">
                    <Link to="/">
                      <ButtonContainer>
                        Back To Products
                      </ButtonContainer>
                    </Link>
                    <ButtonContainer
                      cart
                      disabled={inCart ? true : false}
                      onClick={() => {
                        value.addToCart(id);
                        value.openModal(id);
                      }}>
                      {inCart ? 'InCart' : "Add To Cart"}
                    </ButtonContainer>
                  </div>
                </div>
              </div>
            </div>
          )
        }}
      </ProductConsumer>
    );
  }
}

export default Detail;
