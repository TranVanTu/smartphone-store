import React, { Component } from 'react';
import styled from 'styled-components';
import { ProductConsumer } from '../context';
import { ButtonContainer } from './Button';
import { Link } from 'react-router-dom';
export class Modal extends Component {
  render() {
    return (
      <ProductConsumer>
        {(value) => {
          const { modalOpen, closeModal, addToCart } = value;
          const { price, img, title, id } = value.modalProduct;
          if (modalOpen)
          {
            return (
              <ModalContainer>
                <div className="container bg-white w-50 p-5 rounded">
                  <div className="row">
                    <div id="modal" className="col-8 mx-auto col-lg-6 text-center">
                      <h5>Item is added to the cart</h5>
                      <img src={img} alt={title} />
                      <p>
                        {title}
                      </p>
                      <h5 className="font-weight-bold text-danger">
                        Price :  ${price}
                      </h5>
                      <Link to="/">
                        <ButtonContainer onClick={() => closeModal()} >
                          Continue Shopping
                        </ButtonContainer>
                      </Link>
                      <br />
                      <br />
                      <Link to="/cart">
                        <ButtonContainer cart onClick={() => {
                          closeModal();
                        }} >
                          Add To Cart
                        </ButtonContainer>
                      </Link>
                    </div>
                  </div>
                </div>
              </ModalContainer>)
          } else
          {
            return null;
          }
        }}
      </ProductConsumer>
    )
  }
}

const ModalContainer = styled.div`
  position : fixed;
  top : 0;
  left : 0;
  right : 0;
  bottom : 0;
  background-color : rgba(0,0,0,0.3);
  display : flex;
  align-items : center;
  justify-content : center;
  line-height : 1.8 !important;
  img {
    width : 150px;
  }
`;

export default Modal;
